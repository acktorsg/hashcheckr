# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Prevent ProGuard from removing Fragments that are loading using FragmentContainerView
-keep,allowoptimization public class sg.acktor.hashcheckr.features.settings.SettingsFragment
-keep,allowoptimization public class sg.acktor.hashcheckr.features.createjob.CreateJobFragment
-keep,allowoptimization public class sg.acktor.hashcheckr.features.joblist.JobListFragment
