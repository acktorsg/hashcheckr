package sg.acktor.hashcheckr

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import sg.acktor.hashcheckr.common.CHANNEL_HASHJOB_SERVICE
import sg.acktor.hashcheckr.common.KEY_UI_THEME

class HashCheckrApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        // Re-set night mode
        val optionsUiThemes = resources.getStringArray(R.array.options_uiTheme)
        AppCompatDelegate.setDefaultNightMode(
                when (PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(KEY_UI_THEME, optionsUiThemes[2])) {
                    optionsUiThemes[0] -> AppCompatDelegate.MODE_NIGHT_NO
                    optionsUiThemes[1] -> AppCompatDelegate.MODE_NIGHT_YES
                    else               -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                }
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create notification channel in API 26 & above if not set
            val notificationManager = NotificationManagerCompat.from(this)
            if (notificationManager.notificationChannels.none { it.id == CHANNEL_HASHJOB_SERVICE }) {
                notificationManager.createNotificationChannel(
                        NotificationChannel(
                                CHANNEL_HASHJOB_SERVICE,
                                getString(R.string.name_notificationChannel_hashjobService),
                                NotificationManager.IMPORTANCE_LOW
                        )
                )
            }
        }
    }
}