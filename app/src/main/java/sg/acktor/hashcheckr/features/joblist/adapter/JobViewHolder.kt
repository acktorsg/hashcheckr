package sg.acktor.hashcheckr.features.joblist.adapter

import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import sg.acktor.acktorsdk.ui.recyclerview.BaseViewHolder
import sg.acktor.hashcheckr.R
import sg.acktor.hashcheckr.common.entities.GenerateJobInfo
import sg.acktor.hashcheckr.common.entities.JobInfo
import sg.acktor.hashcheckr.common.entities.ValidateJobInfo
import sg.acktor.hashcheckr.databinding.RowJobBinding
import sg.acktor.hashcheckr.common.jobsubscription.JobSubscriber
import sg.acktor.hashcheckr.features.generatehash.TaskProgress
import java.io.FileNotFoundException
import java.io.IOException

class JobViewHolder(container: ViewGroup) : BaseViewHolder<JobInfo, RowJobBinding>(
    container, R.layout.row_job
) {
    override val viewBinding = RowJobBinding.bind(itemView)

    private val disposables = CompositeDisposable()

    init {
        viewBinding.apply {
            applyListenerToViews(buttonCopyHashSum, buttonRedoHash, buttonRetry, buttonRemoveJob)
        }
    }

    override fun RowJobBinding.onBindHolder() {
        val job = obj

        // Reset error message
        layoutError.visibility = View.GONE

        // Show job details
        textViewFileName.text = job.fileName
        chipHashAlgorithm.text = job.algorithm

        if (job is ValidateJobInfo) {
            textViewInput.text = job.hashSum
        } else {
            textViewInput.visibility = View.GONE
        }

        // Process hashing result
        val hashSumResult = job.generatedHashSum
        if (hashSumResult != null) {
            progressBarTask.visibility = View.GONE

            when (job) {
                is GenerateJobInfo -> textViewResult.text = hashSumResult
                is ValidateJobInfo -> {
                    val (resultTextRes, resultColorRes) = if (
                        job.hashSum.equals(hashSumResult, true)
                    ) {
                        R.string.message_success_hashSumMatch to R.color.green_500
                    } else R.string.message_failure_hashSumMatch to R.color.red_500

                    textViewResult.setText(resultTextRes)
                    textViewResult.setTextColor(ContextCompat.getColor(context, resultColorRes))
                }
            }

            return
        }

        groupResult.visibility = View.GONE

        val subscription = JobSubscriber.getSubscription(obj.id) ?: return
        subscription.events.observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<TaskProgress> {
                override fun onSubscribe(d: Disposable) {
                    disposables.addAll(d)
                }

                override fun onNext(t: TaskProgress) {
                    if (progressBarTask.max != t.total) {
                        progressBarTask.max = t.total
                    }

                    progressBarTask.progress = t.progress
                }

                override fun onComplete() {
                    progressBarTask.progress = 0
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()

                    textViewError.setText(
                        when (e) {
                            is FileNotFoundException -> R.string.message_error_fileNotFound
                            is IOException -> R.string.message_error_fileCannotRead
                            else -> R.string.message_error_fileHash
                        }
                    )

                    layoutError.visibility = View.VISIBLE
                    progressBarTask.visibility = View.GONE
                }
            })
    }

    override fun onRecycled() {
        disposables.clear()

        // Reset views
        viewBinding.apply {
            textViewInput.visibility = View.VISIBLE
            textViewLabelResult.visibility = View.VISIBLE
            progressBarTask.visibility = View.VISIBLE
            textViewResult.visibility = View.VISIBLE
            buttonCopyHashSum.visibility = View.VISIBLE
            layoutError.visibility = View.GONE
            groupResult.visibility = View.VISIBLE
        }
    }
}
