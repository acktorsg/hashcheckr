package sg.acktor.hashcheckr.features.joblist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import sg.acktor.hashcheckr.common.JobRepository
import sg.acktor.hashcheckr.common.entities.EntityId
import sg.acktor.hashcheckr.common.entities.JobInfo
import sg.acktor.hashcheckr.common.entities.JobType
import sg.acktor.hashcheckr.common.jobsubscription.JobSubscriber

class JobListViewModel(app: Application) : AndroidViewModel(app) {
    private val dao = JobRepository.getDatabase(app).jobListDao()

    fun getAllJobs(): Observable<List<JobInfo>> = dao.getAllJobs()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun redoHash(jobId: Long, jobType: JobType, fileId: Long, algorithm: String): Completable =
        dao.resetGeneratedHashSum(fileId, algorithm)
            .doOnComplete { retryJob(jobId, jobType) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun retryJob(jobId: Long, jobType: JobType) {
        JobSubscriber.sendJobToService(getApplication(), jobId, jobType)
    }

    fun removeJob(jobId: Long, jobType: JobType, fileId: Long): Completable {
        val jobIdObj = EntityId(jobId)

        return dao.removeJob(jobIdObj, jobType)
            .andThen(dao.removeFileIfUnlinked(fileId))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
