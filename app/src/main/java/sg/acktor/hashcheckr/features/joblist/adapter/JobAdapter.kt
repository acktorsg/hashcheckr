package sg.acktor.hashcheckr.features.joblist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import sg.acktor.acktorsdk.ui.recyclerview.BaseListAdapter
import sg.acktor.hashcheckr.common.entities.JobInfo

class JobAdapter : BaseListAdapter<JobInfo>(object : DiffUtil.ItemCallback<JobInfo>() {
    override fun areItemsTheSame(oldItem: JobInfo, newItem: JobInfo) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: JobInfo, newItem: JobInfo) =
        oldItem.areContentsTheSame(newItem)
}) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = JobViewHolder(parent)
}
