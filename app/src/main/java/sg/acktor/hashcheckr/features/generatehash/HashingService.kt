package sg.acktor.hashcheckr.features.generatehash

import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import sg.acktor.hashcheckr.R
import sg.acktor.hashcheckr.common.CHANNEL_HASHJOB_SERVICE
import sg.acktor.hashcheckr.common.JobRepository
import sg.acktor.hashcheckr.common.KEY_HASHJOB_ID
import sg.acktor.hashcheckr.common.KEY_JOB_TYPE
import sg.acktor.hashcheckr.common.entities.GeneratedHashSum
import sg.acktor.hashcheckr.common.entities.JobRunner
import sg.acktor.hashcheckr.common.entities.JobType
import sg.acktor.hashcheckr.common.extensions.checkFileAvailability
import sg.acktor.hashcheckr.common.jobsubscription.JobSubscriber
import java.util.concurrent.TimeUnit

class HashingService : Service() {
    private lateinit var dao: HashEngineDAO
    private lateinit var notificationManager: NotificationManagerCompat
    private val disposables = CompositeDisposable()

    override fun onCreate() {
        dao = JobRepository.getDatabase(this).hashEngineDao()
        notificationManager = NotificationManagerCompat.from(this)
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val jobId = intent?.getLongExtra(KEY_HASHJOB_ID, 0) ?: 0
        if (jobId == 0L) {
            return if (JobSubscriber.hasNoPendingSubscription()) START_NOT_STICKY
            else super.onStartCommand(intent, flags, startId)
        }

        val jobTypeInt = intent?.getIntExtra(KEY_JOB_TYPE, -1) ?: -1
        if (jobTypeInt == -1) {
            return if (JobSubscriber.hasNoPendingSubscription()) START_NOT_STICKY
            else super.onStartCommand(intent, flags, startId)
        }

        disposables.add(
            dao.getJob(jobId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { onJobRetrieved(it, JobType.values()[jobTypeInt]) },
                    {
                        it.printStackTrace()
                        clearDisposables()
                    },
                    this::clearDisposables
                )
        )

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        JobSubscriber.clearSubscriptions()
        disposables.clear()
    }

    private fun onJobRetrieved(job: JobRunner, jobType: JobType) {
        // Create subscription
        val subscription = JobSubscriber.getSubscription(job.id) ?: return

        // Show notification
        val notificationId = (job.id + 1).toInt()
        val notificationBuilder = NotificationCompat.Builder(this, CHANNEL_HASHJOB_SERVICE)
            .setSmallIcon(R.drawable.ic_notification)
            .setOngoing(true)
            .setContentTitle(
                getString(
                    when (jobType) {
                        JobType.GENERATE -> R.string.message_notification_hash
                        JobType.VALIDATE -> R.string.message_notification_check
                    }, job.fileName
                )
            )
            .setContentText(getString(R.string.message_notification_waiting))

        notificationManager.notify(notificationId, notificationBuilder.build())

        // File check before execution
        val fileCheckException = checkFileAvailability(job.fileUri)
        if (fileCheckException != null) {
            subscription.publishError(fileCheckException)
            clearJobFromService(notificationId)
            return
        }

        HashEngine.runJob(contentResolver, subscription, job)

        // Get job updates
        subscription.events.throttleLast(1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<TaskProgress> {
                override fun onSubscribe(d: Disposable?) {
                    if (d == null) return
                    disposables.add(d)
                }

                override fun onNext(t: TaskProgress?) {
                    if (t == null) return

                    notificationBuilder.setProgress(t.total, t.progress, false)
                    notificationManager.notify(
                        notificationId,
                        notificationBuilder.build()
                    )
                }

                override fun onError(e: Throwable?) {
                    e?.printStackTrace()
                    clearJobFromService(notificationId)
                }

                override fun onComplete() {
                    dao.addGeneratedHashSum(
                        GeneratedHashSum(job.fileId, job.algorithm, job.generatedHashSum ?: "")
                    )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object : CompletableObserver {
                            override fun onSubscribe(d: Disposable?) {
                                if (d == null) return
                                disposables.add(d)
                            }

                            override fun onComplete() {
                                clearJobFromService(notificationId)
                            }

                            override fun onError(e: Throwable?) {
                                e?.printStackTrace()
                                clearJobFromService(notificationId)
                            }
                        })
                }
            })
    }

    private fun clearJobFromService(notificationId: Int) {
        notificationManager.cancel(notificationId)
        clearDisposables()
    }

    private fun clearDisposables() {
        if (JobSubscriber.hasNoPendingSubscription()) {
            disposables.clear()
        }
    }
}
