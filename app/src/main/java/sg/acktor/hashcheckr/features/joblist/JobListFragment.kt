package sg.acktor.hashcheckr.features.joblist

import android.content.ClipData
import android.content.ClipboardManager
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Px
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.viewModels
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import sg.acktor.acktorsdk.ui.BaseFragment
import sg.acktor.hashcheckr.R
import sg.acktor.hashcheckr.common.LABEL_HASHSUM
import sg.acktor.hashcheckr.common.entities.GenerateJobInfo
import sg.acktor.hashcheckr.common.entities.JobInfo
import sg.acktor.hashcheckr.common.entities.JobType
import sg.acktor.hashcheckr.common.entities.ValidateJobInfo
import sg.acktor.hashcheckr.common.extensions.mainActivity
import sg.acktor.hashcheckr.databinding.FragmentJobListBinding
import sg.acktor.hashcheckr.features.joblist.adapter.JobAdapter

class JobListFragment : BaseFragment<FragmentJobListBinding>() {
    private val jobAdapter = JobAdapter()
    private val removeJobDialog by lazy {
        AlertDialog.Builder(requireContext())
            .setMessage(R.string.message_confirm_removeJob)
            .setNegativeButton(R.string.title_button_cancel, null)
            .create()
    }

    private val jobListViewModel by viewModels<JobListViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        jobAdapter.setClickListener { view, _, job ->
            when (view.id) {
                R.id.buttonCopyHashSum -> {
                    ContextCompat.getSystemService(requireContext(), ClipboardManager::class.java)
                        ?.setPrimaryClip(
                            ClipData.newPlainText(
                                LABEL_HASHSUM, when (job) {
                                    is GenerateJobInfo -> job.generatedHashSum ?: ""
                                    is ValidateJobInfo -> job.hashSum
                                }
                            )
                        )

                    mainActivity.showMessage(getString(R.string.message_success_copyHashSum))
                }
                R.id.buttonRedoHash -> {
                    jobListViewModel.redoHash(
                        job.id, when (job) {
                            is GenerateJobInfo -> JobType.GENERATE
                            is ValidateJobInfo -> JobType.VALIDATE
                        }, job.fileId, job.algorithm
                    )
                        .subscribe(object : CompletableObserver {
                            override fun onSubscribe(d: Disposable?) {
                                disposables.add(d)
                            }

                            override fun onComplete() {}

                            override fun onError(e: Throwable?) {
                                e?.printStackTrace()
                                mainActivity.showMessage(getString(R.string.message_error_retryJob))
                            }
                        })
                }
                R.id.buttonRetry -> jobListViewModel.retryJob(
                    job.id, when (job) {
                        is GenerateJobInfo -> JobType.GENERATE
                        is ValidateJobInfo -> JobType.VALIDATE
                    }
                )
                R.id.buttonRemoveJob -> {
                    removeJobDialog.setButton(
                        DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.title_button_remove)
                    ) { _, _ ->
                        jobListViewModel.removeJob(
                            job.id, when (job) {
                                is GenerateJobInfo -> JobType.GENERATE
                                is ValidateJobInfo -> JobType.VALIDATE
                            }, job.fileId
                        ).subscribe(object : CompletableObserver {
                            override fun onSubscribe(d: Disposable?) {
                                disposables.add(d)
                            }

                            override fun onComplete() {
                                mainActivity.showMessage(getString(R.string.message_success_removeJob))
                            }

                            override fun onError(e: Throwable?) {
                                e?.printStackTrace()
                                mainActivity.showMessage(getString(R.string.message_error_removeJob))
                            }
                        })
                    }

                    removeJobDialog.show()
                }
            }
        }
    }

    override fun createViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentJobListBinding.inflate(inflater, container, false)

    override fun FragmentJobListBinding.initViews(savedInstanceState: Bundle?) {
        recyclerViewTasks.adapter = jobAdapter
    }

    override fun onStart() {
        super.onStart()

        // Subscribe to events
        jobListViewModel.getAllJobs().subscribe(object : Observer<List<JobInfo>> {
            override fun onSubscribe(d: Disposable) {
                disposables.add(d)
            }

            override fun onNext(t: List<JobInfo>?) {
                if (t == null) return

                jobAdapter.submitList(t)
                if (t.isNotEmpty()) {
                    viewBinding.textViewNoJobs.visibility = View.GONE
                }
            }

            override fun onComplete() {}

            override fun onError(e: Throwable) {
                e.printStackTrace()
                mainActivity.showMessage(getString(R.string.message_error_getJobs))
            }
        })
    }

    fun addPaddingToList(@Px height: Int) {
        viewBinding.recyclerViewTasks.updatePadding(bottom = height)
    }
}
