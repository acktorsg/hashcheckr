package sg.acktor.hashcheckr.features.joblist

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import sg.acktor.hashcheckr.common.entities.EntityId
import sg.acktor.hashcheckr.common.entities.File
import sg.acktor.hashcheckr.common.entities.FileHashSumDelete
import sg.acktor.hashcheckr.common.entities.GenerateJob
import sg.acktor.hashcheckr.common.entities.GenerateJobInfo
import sg.acktor.hashcheckr.common.entities.GeneratedHashSum
import sg.acktor.hashcheckr.common.entities.HashingJob
import sg.acktor.hashcheckr.common.entities.JobInfo
import sg.acktor.hashcheckr.common.entities.JobType
import sg.acktor.hashcheckr.common.entities.ValidateJob
import sg.acktor.hashcheckr.common.entities.ValidateJobInfo

@Dao
interface JobListDAO {
    fun getAllJobs(): Observable<List<JobInfo>> = Observable.combineLatest(
        getGenerateJobs(), getValidateJobs(), { generateJobs, validateJobs ->
            listOf(*generateJobs, *validateJobs).sortedByDescending(JobInfo::creationDateTime)
        }
    )

    @Query(
        """
        SELECT gj.id, h.algorithm, h.creationDateTime, f.id AS fileId, f.name AS fileName, gsh.generatedHashSum
        FROM GenerateJob gj
        INNER JOIN HashingJob h USING(id)
        INNER JOIN File f ON h.fileId = f.id
        LEFT JOIN GeneratedHashSum gsh ON h.fileId = gsh.fileId AND h.algorithm = gsh.algorithm
        ORDER BY h.creationDateTime DESC
        """
    )
    fun getGenerateJobs(): Observable<Array<GenerateJobInfo>>

    @Query(
        """
        SELECT vj.id, h.algorithm, h.creationDateTime, vj.hashSum, f.id AS fileId, f.name AS fileName, gsh.generatedHashSum
        FROM ValidateJob vj
        INNER JOIN HashingJob h on vj.id = h.id
        INNER JOIN File f ON h.fileId = f.id
        LEFT JOIN GeneratedHashSum gsh ON h.fileId = gsh.fileId AND h.algorithm = gsh.algorithm
        ORDER BY h.creationDateTime DESC
        """
    )
    fun getValidateJobs(): Observable<Array<ValidateJobInfo>>

    fun resetGeneratedHashSum(fileId: Long, algorithm: String): Completable =
        deleteGeneratedHashSum(FileHashSumDelete(fileId, algorithm))

    @Delete(entity = GeneratedHashSum::class)
    fun deleteGeneratedHashSum(file: FileHashSumDelete): Completable

    fun removeJob(jobId: EntityId, jobType: JobType): Completable = Completable.fromAction {
        removeJobImpl(jobId)
        when (jobType) {
            JobType.GENERATE -> removeGenerateJob(jobId)
            JobType.VALIDATE -> removeValidateJob(jobId)
        }
    }

    @Delete(entity = HashingJob::class)
    fun removeJobImpl(job: EntityId)

    @Delete(entity = GenerateJob::class)
    fun removeGenerateJob(job: EntityId)

    @Delete(entity = ValidateJob::class)
    fun removeValidateJob(job: EntityId)

    fun removeFileIfUnlinked(fileId: Long): Completable = Completable.fromAction {
        if (fileHasLinks(fileId)) return@fromAction
        removeFile(EntityId(fileId))
    }

    @Query("SELECT COUNT(fileId) > 0 FROM HashingJob h WHERE fileId = :fileId")
    fun fileHasLinks(fileId: Long): Boolean

    @Delete(entity = File::class)
    fun removeFile(file: EntityId)
}
