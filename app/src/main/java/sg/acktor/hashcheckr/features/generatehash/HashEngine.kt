package sg.acktor.hashcheckr.features.generatehash

import android.content.ContentResolver
import android.os.Build
import sg.acktor.hashcheckr.common.entities.JobRunner
import sg.acktor.hashcheckr.common.extensions.toHexString
import sg.acktor.hashcheckr.common.jobsubscription.JobSubscription
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.Executors

object HashEngine {
    // SHA-224 not supported on API 21
    val hashAlgorithms = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1)
        arrayOf(
            HashAlgorithm("MD5", 32),
            HashAlgorithm("SHA-1", 40),
            HashAlgorithm("SHA-256", 64),
            HashAlgorithm("SHA-384", 96),
            HashAlgorithm("SHA-512", 128),
            HashAlgorithm("SHA-224", 56)
        )
    else
        arrayOf(
            HashAlgorithm("MD5", 32),
            HashAlgorithm("SHA-1", 40),
            HashAlgorithm("SHA-256", 64),
            HashAlgorithm("SHA-384", 96),
            HashAlgorithm("SHA-512", 128)
        )

    private val executor = Executors.newFixedThreadPool(1)

    fun checkAllAlgorithmsExist(): List<String> {
        return hashAlgorithms.filter { algorithm ->
            try {
                MessageDigest.getInstance(algorithm.name)
                false
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
                true
            }
        }.map(HashAlgorithm::name)
    }

    fun runJob(contentResolver: ContentResolver, subscription: JobSubscription, job: JobRunner) {
        // Run job on separate thread
        executor.execute {
            var result: String? = null

            val messageDigest = MessageDigest.getInstance(job.algorithm)
            val bufferedStream = contentResolver.openInputStream(job.fileUri)
                ?.buffered(DEFAULT_BUFFER_SIZE)
            if (bufferedStream == null) {
                subscription.publishError(IOException())
                return@execute
            }

            val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
            var totalRead = 0
            try {
                val totalFileSize = bufferedStream.available()

                var numRead: Int = bufferedStream.read(buffer)
                while (numRead > 0) {
                    messageDigest.update(buffer, 0, numRead)
                    totalRead += numRead

                    subscription.publishProgress(TaskProgress(totalRead, totalFileSize))

                    numRead = bufferedStream.read(buffer)
                }

                result = messageDigest.digest().toHexString()
            } catch (e: IOException) {
                subscription.publishError(e)
            } finally {
                try {
                    bufferedStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                job.generatedHashSum = result
                subscription.markAsComplete()
            }
        }
    }
}
