package sg.acktor.hashcheckr.features.missingalgorithms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import sg.acktor.acktorsdk.ui.BaseFragment
import sg.acktor.hashcheckr.common.KEY_MISSING_ALGORITHMS
import sg.acktor.hashcheckr.R
import sg.acktor.hashcheckr.databinding.FragmentMissingAlgorithmBinding

class MissingAlgorithmFragment : BaseFragment<FragmentMissingAlgorithmBinding>() {
    companion object {
        const val TAG = "MissingAlgorithmFragment"

        private val ARGS = Bundle(1)

        fun create(missingAlgorithms: Array<String>): MissingAlgorithmFragment {
            val fragment = MissingAlgorithmFragment()
            ARGS.putStringArray(KEY_MISSING_ALGORITHMS, missingAlgorithms)
            fragment.arguments = ARGS

            return fragment
        }
    }

    override fun createViewBinding(inflater: LayoutInflater,
                                   container: ViewGroup?): FragmentMissingAlgorithmBinding =
            FragmentMissingAlgorithmBinding.inflate(inflater, container, false)

    override fun FragmentMissingAlgorithmBinding.initViews(savedInstanceState: Bundle?) {
        textViewMissingAlgorithmList.text = getString(
                R.string.message_missing_algorithm_list,
                arguments?.getStringArray(KEY_MISSING_ALGORITHMS)?.reduce { acc, s -> "$acc, $s" }
        )
    }
}