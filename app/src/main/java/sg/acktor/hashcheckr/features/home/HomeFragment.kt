package sg.acktor.hashcheckr.features.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import sg.acktor.acktorsdk.ui.BaseFragment
import sg.acktor.hashcheckr.R
import sg.acktor.hashcheckr.common.extensions.mainActivity
import sg.acktor.hashcheckr.databinding.FragmentHomeBinding
import sg.acktor.hashcheckr.features.joblist.JobListFragment

class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    companion object {
        const val TAG = "HomeFragment"
    }

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<*>
    private var hiddenHeight: Int = -1
    private var expandedHeight: Int = -1

    private val jobListFragment: JobListFragment?
        get() = childFragmentManager.findFragmentById(R.id.fragmentHolder) as? JobListFragment

    private val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {}

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            viewBinding.buttonExpand.rotation = -180f * slideOffset

            if (hiddenHeight == -1 || expandedHeight == -1) return
            jobListFragment?.addPaddingToList(
                (expandedHeight * slideOffset + hiddenHeight * (1.0 - slideOffset)).toInt()
            )
        }
    }

    override fun createViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentHomeBinding.inflate(inflater, container, false)

    override fun FragmentHomeBinding.initViews(savedInstanceState: Bundle?) {
        bottomSheetBehavior = BottomSheetBehavior.from(layoutCreateJob)

        toolbarHome.inflateMenu(R.menu.menu_toolbar_home)
        toolbarHome.setOnMenuItemClickListener { item: MenuItem ->
            when (item.itemId) {
                R.id.menuSettings -> {
                    mainActivity.showSettingsUi()
                    true
                }
                R.id.menuAbout -> {
                    mainActivity.showAboutUi()
                    true
                }
                else -> false
            }
        }

        // Prevents touch events on create job UI from being passed to job list
        layoutCreateJob.setOnClickListener {}

        buttonExpand.setOnClickListener {
            when (bottomSheetBehavior.state) {
                BottomSheetBehavior.STATE_EXPANDED -> {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                }
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        }

        textViewCreateJobTitle.doOnLayout {
            hiddenHeight =
                it.height + (2 * resources.getDimensionPixelSize(R.dimen.spacing_default))

            bottomSheetBehavior.peekHeight = hiddenHeight
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        layoutCreateJob.doOnLayout { expandedHeight = it.height }

        bottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)
    }

    override fun onDestroyView() {
        bottomSheetBehavior.removeBottomSheetCallback(bottomSheetCallback)
        super.onDestroyView()
    }
}
