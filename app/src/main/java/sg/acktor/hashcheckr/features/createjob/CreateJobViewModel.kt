package sg.acktor.hashcheckr.features.createjob

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import sg.acktor.hashcheckr.common.JobRepository
import sg.acktor.hashcheckr.common.entities.JobType
import sg.acktor.hashcheckr.common.jobsubscription.JobSubscriber
import sg.acktor.hashcheckr.features.generatehash.HashEngine

private val EMPTY_FILE = Triple("", Uri.EMPTY, 0L)

class CreateJobViewModel(app: Application) : AndroidViewModel(app) {
    private val dao = JobRepository.getDatabase(app).createJobDao()

    val algoPosSubject: BehaviorSubject<Int> = BehaviorSubject.createDefault(0)
    val fileSubject: BehaviorSubject<Triple<String, Uri, Long>> =
        BehaviorSubject.createDefault(EMPTY_FILE)
    val hashSumSubject: BehaviorSubject<String> = BehaviorSubject.createDefault("")

    fun clearFile() = fileSubject.onNext(EMPTY_FILE)

    fun getSubmitEnabledState(): Observable<Boolean> = Observable.combineLatest(
        algoPosSubject, fileSubject, hashSumSubject, JobValidator::validateJob
    ).distinctUntilChanged()

    fun createJob(): Completable = Completable
        .fromAction {
            val (fileName, uri, lastModified) = fileSubject.value
            val fileId = dao.addFileIfNotExists(fileName, uri, lastModified)

            val algorithm = HashEngine.hashAlgorithms[algoPosSubject.value].name
            val hashSum = hashSumSubject.value

            val jobId = dao.addJob(algorithm, fileId, hashSum)
            val jobType = if (hashSum.isBlank()) JobType.GENERATE else JobType.VALIDATE

            if (dao.shouldGenerateHashSum(fileId, algorithm, lastModified)) {
                JobSubscriber.sendJobToService(getApplication(), jobId, jobType)
            }
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}
