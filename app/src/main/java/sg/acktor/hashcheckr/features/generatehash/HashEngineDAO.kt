package sg.acktor.hashcheckr.features.generatehash

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import sg.acktor.hashcheckr.common.entities.GeneratedHashSum
import sg.acktor.hashcheckr.common.entities.JobRunner

@Dao
interface HashEngineDAO {
    @Query(
        """
        SELECT h.id, h.algorithm, h.fileId, f.name AS fileName, f.uri AS fileUri, NULL AS generatedHashSum
        FROM HashingJob h
        INNER JOIN File f ON h.fileId = f.id
        WHERE h.id = :jobId
        """
    )
    fun getJob(jobId: Long): Maybe<JobRunner>

    @Insert
    fun addGeneratedHashSum(generatedHashSum: GeneratedHashSum): Completable
}
