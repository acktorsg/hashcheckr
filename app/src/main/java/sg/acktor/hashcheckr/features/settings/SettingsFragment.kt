package sg.acktor.hashcheckr.features.settings

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.Preference
import sg.acktor.acktorsdk.ui.BasePreferenceFragment
import sg.acktor.hashcheckr.common.KEY_UI_THEME
import sg.acktor.hashcheckr.R

class SettingsFragment : BasePreferenceFragment() {
    companion object {
        const val TAG = "SettingsFragment"
    }

    override val preferenceRes = R.xml.preferences

    private lateinit var optionsUiThemes: Array<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        optionsUiThemes = resources.getStringArray(R.array.options_uiTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findPreference<Preference>(KEY_UI_THEME)?.setOnPreferenceChangeListener { _, newValue ->
            AppCompatDelegate.setDefaultNightMode(
                    when (newValue) {
                        optionsUiThemes[0] -> AppCompatDelegate.MODE_NIGHT_NO
                        optionsUiThemes[1] -> AppCompatDelegate.MODE_NIGHT_YES
                        else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                    }
            )

            return@setOnPreferenceChangeListener true
        }
    }
}