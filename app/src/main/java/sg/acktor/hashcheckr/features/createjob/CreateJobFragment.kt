package sg.acktor.hashcheckr.features.createjob

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import sg.acktor.acktorsdk.file.FilePermissionManager
import sg.acktor.acktorsdk.file.FileSelectionManager
import sg.acktor.acktorsdk.ui.BaseFragment
import sg.acktor.hashcheckr.R
import sg.acktor.hashcheckr.common.extensions.mainActivity
import sg.acktor.hashcheckr.databinding.FragmentCreateJobBinding
import sg.acktor.hashcheckr.features.generatehash.HashAlgorithm
import sg.acktor.hashcheckr.features.generatehash.HashEngine

class CreateJobFragment : BaseFragment<FragmentCreateJobBinding>() {
    private val createJobViewModel by viewModels<CreateJobViewModel>()
    private val filePermissionRationaleDialog by lazy {
        AlertDialog.Builder(requireContext())
            .setMessage(R.string.message_dialog_filePermissionDenied)
            .setPositiveButton(R.string.title_button_request) { _, _ ->
                filePermissionManager.requestPermission(false)
            }
            .create()
    }

    private val filePermissionManager = FilePermissionManager(this)
    private val fileSelectionManager = FileSelectionManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        filePermissionManager.setPermissionGrantListener { isGranted ->
            if (!isGranted) {
                if (!filePermissionRationaleDialog.isShowing) {
                    filePermissionRationaleDialog.show()
                }

                return@setPermissionGrantListener
            }

            fileSelectionManager.showFilePicker(requireContext(), "*/*")
        }

        fileSelectionManager.setFileSelectedListener { documentFile ->
            if (documentFile == null) {
                mainActivity.showMessage(getString(R.string.message_error_fileSelect))
                return@setFileSelectedListener
            }

            val fileName = documentFile.name ?: run {
                mainActivity.showMessage(getString(R.string.message_error_fileSelect))
                return@setFileSelectedListener
            }

            val lastModified = documentFile.lastModified()
            if (lastModified == 0L) {
                mainActivity.showMessage(getString(R.string.message_error_fileSelect))
                return@setFileSelectedListener
            }

            val uri = documentFile.uri

            createJobViewModel.fileSubject.onNext(Triple(fileName, uri, lastModified))

            viewBinding.apply {
                textViewFilePath.text = fileName
                buttonClearFile.visibility = View.VISIBLE
                buttonSelectFile.visibility = View.GONE
            }
        }
    }

    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentCreateJobBinding = FragmentCreateJobBinding.inflate(inflater, container, false)

    override fun FragmentCreateJobBinding.initViews(savedInstanceState: Bundle?) {
        buttonSelectFile.setOnClickListener {
            editTextHashSum.clearFocus()
            filePermissionManager.requestPermission(false)
        }

        buttonClearFile.setOnClickListener {
            createJobViewModel.clearFile()
            viewBinding.textViewFilePath.setText(R.string.message_empty_file)
            buttonSelectFile.visibility = View.VISIBLE
            buttonClearFile.visibility = View.GONE
        }

        editTextHashSum.doOnTextChanged { text, _, _, _ ->
            createJobViewModel.hashSumSubject.onNext(text.toString())

            val pos = HashEngine.hashAlgorithms.indexOfFirst {
                text?.length == it.expectedLength
            }
            if (pos >= 0) {
                spinnerAlgorithms.setSelection(pos, true)
            }
        }

        spinnerAlgorithms.adapter = ArrayAdapter(
            requireContext(), android.R.layout.simple_list_item_1,
            HashEngine.hashAlgorithms.map(HashAlgorithm::name)
        )
        spinnerAlgorithms.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                createJobViewModel.algoPosSubject.onNext(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        buttonSubmitJob.setOnClickListener {
            createJobViewModel.createJob().subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable?) {
                    disposables.add(d)
                }

                override fun onComplete() {
                    buttonClearFile.callOnClick()
                    editTextHashSum.setText("")
                }

                override fun onError(e: Throwable?) {
                    e?.printStackTrace()
                    mainActivity.showMessage(getString(R.string.message_error_createJob))
                }
            })
        }
    }

    override fun onStart() {
        super.onStart()

        createJobViewModel.getSubmitEnabledState().subscribe(object : Observer<Boolean> {
            override fun onSubscribe(d: Disposable?) {
                disposables.add(d)
            }

            override fun onNext(t: Boolean) {
                viewBinding.buttonSubmitJob.isEnabled = t
            }

            override fun onError(e: Throwable?) {
                e?.printStackTrace()
            }

            override fun onComplete() {}
        })
    }

    override fun onStop() {
        // Hide keyboard
        ContextCompat.getSystemService(requireContext(), InputMethodManager::class.java)
            ?.hideSoftInputFromWindow(viewBinding.root.windowToken, 0)
        super.onStop()
    }
}
