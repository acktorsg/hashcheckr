package sg.acktor.hashcheckr.features.createjob

import android.net.Uri
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import sg.acktor.hashcheckr.common.entities.File
import sg.acktor.hashcheckr.common.entities.FileHashSumDelete
import sg.acktor.hashcheckr.common.entities.GenerateJob
import sg.acktor.hashcheckr.common.entities.GeneratedHashSum
import sg.acktor.hashcheckr.common.entities.HashingJob
import sg.acktor.hashcheckr.common.entities.LastModifiedUpdate
import sg.acktor.hashcheckr.common.entities.ValidateJob

@Dao
interface CreateJobDAO {
    fun addFileIfNotExists(fileName: String, uri: Uri, lastModified: Long) = getFileIdByUri(uri)
        ?: addFile(File(name = fileName, uri = uri, lastModified = lastModified))

    @Query("SELECT id FROM File WHERE uri = :uri")
    fun getFileIdByUri(uri: Uri): Long?

    @Insert
    fun addFile(file: File): Long

    @Transaction
    fun addJob(algorithm: String, fileId: Long, hashSum: String): Long {
        val jobId = addJobImpl(HashingJob(algorithm = algorithm, fileId = fileId))
        if (hashSum.isBlank()) {
            addGenerateJob(GenerateJob(jobId))
        } else {
            addValidateJob(ValidateJob(jobId, hashSum))
        }

        return jobId
    }

    @Insert
    fun addJobImpl(job: HashingJob): Long

    @Insert
    fun addGenerateJob(job: GenerateJob)

    @Insert
    fun addValidateJob(job: ValidateJob)

    @Transaction
    fun shouldGenerateHashSum(fileId: Long, algorithm: String, lastModified: Long): Boolean {
        val currentLastModified = getFileLastModified(fileId)
        if (currentLastModified != lastModified) {
            removeGeneratedHashSum(FileHashSumDelete(fileId, algorithm))
            updateLastModified(LastModifiedUpdate(fileId, lastModified))
            return true
        }

        return !hasGeneratedHashSum(fileId, algorithm)
    }

    @Query("SELECT lastModified FROM File WHERE id = :fileId")
    fun getFileLastModified(fileId: Long): Long

    @Delete(entity = GeneratedHashSum::class)
    fun removeGeneratedHashSum(generatedHashSum: FileHashSumDelete)

    @Update(entity = File::class)
    fun updateLastModified(file: LastModifiedUpdate)

    @Query("SELECT COUNT(generatedHashSum) > 0 FROM GeneratedHashSum WHERE fileId = :fileId AND algorithm = :algorithm")
    fun hasGeneratedHashSum(fileId: Long, algorithm: String): Boolean
}
