package sg.acktor.hashcheckr.features.createjob

import android.net.Uri
import sg.acktor.hashcheckr.common.extensions.isHexString
import sg.acktor.hashcheckr.features.generatehash.HashAlgorithm
import sg.acktor.hashcheckr.features.generatehash.HashEngine

object JobValidator {
    fun validateJob(algoPos: Int, file: Triple<String, Uri, Long>, hashSum: String): Boolean {
        if (file.first.isBlank() || file.second == Uri.EMPTY || file.third == 0L) {
            return false
        }

        return isValidHashSum(HashEngine.hashAlgorithms[algoPos], hashSum)
    }

    private fun isValidHashSum(
        algorithm: HashAlgorithm, hashSum: String
    ) = hashSum.isBlank() || (hashSum.length == algorithm.expectedLength && hashSum.isHexString())
}
