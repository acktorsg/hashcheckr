package sg.acktor.hashcheckr.features.generatehash

data class HashAlgorithm(val name: String, val expectedLength: Int)