package sg.acktor.hashcheckr.common.jobsubscription

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import sg.acktor.hashcheckr.features.generatehash.TaskProgress
import java.util.concurrent.TimeUnit

class JobSubscription(val jobId: Long) {
    private var eventSubject = PublishSubject.create<TaskProgress>()
    val events: Observable<TaskProgress>
        get() = eventSubject.publish().autoConnect()
                .throttleLast(1000 / 120, TimeUnit.MILLISECONDS)

    fun publishProgress(progress: TaskProgress) = eventSubject.onNext(progress)

    fun markAsComplete() = eventSubject.onComplete()

    fun publishError(e: Throwable) = eventSubject.onError(e)

    fun hasJobCompleted() = eventSubject.hasComplete()

    fun resetEvents() {
        eventSubject = PublishSubject.create()
    }
}
