package sg.acktor.hashcheckr.common.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import sg.acktor.hashcheckr.common.entities.File
import sg.acktor.hashcheckr.common.entities.GenerateJob
import sg.acktor.hashcheckr.common.entities.GeneratedHashSum
import sg.acktor.hashcheckr.common.entities.HashingJob
import sg.acktor.hashcheckr.common.entities.ValidateJob
import sg.acktor.hashcheckr.features.createjob.CreateJobDAO
import sg.acktor.hashcheckr.features.generatehash.HashEngineDAO
import sg.acktor.hashcheckr.features.joblist.JobListDAO

@Database(
    entities = [HashingJob::class, GenerateJob::class, ValidateJob::class, GeneratedHashSum::class, File::class],
    version = 3
)
@TypeConverters(UriConverter::class, DateTimeConverter::class)
abstract class HashCheckrDatabase : RoomDatabase() {
    abstract fun createJobDao(): CreateJobDAO

    abstract fun hashEngineDao(): HashEngineDAO

    abstract fun jobListDao(): JobListDAO
}
