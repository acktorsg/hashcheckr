package sg.acktor.hashcheckr.common.extensions

fun String.isHexString() = if (isNotBlank()) {
    toCharArray().all { it in '0'..'9' || it in 'a'..'f' || it in 'A'..'F' }
} else false