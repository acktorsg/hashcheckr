package sg.acktor.hashcheckr.common.extensions

fun ByteArray.toHexString() = joinToString(separator = "") { byte ->
    Integer.toHexString(0xFF and byte.toInt()).padStart(2, '0')
}