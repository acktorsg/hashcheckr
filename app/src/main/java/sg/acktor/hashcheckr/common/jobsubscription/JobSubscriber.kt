package sg.acktor.hashcheckr.common.jobsubscription

import android.content.Context
import android.content.Intent
import sg.acktor.hashcheckr.common.KEY_HASHJOB_ID
import sg.acktor.hashcheckr.common.KEY_JOB_TYPE
import sg.acktor.hashcheckr.common.entities.JobType
import sg.acktor.hashcheckr.features.generatehash.HashingService

object JobSubscriber {
    private val subscriptions = mutableListOf<JobSubscription>()

    fun getSubscription(jobId: Long) = subscriptions.find { it.jobId == jobId }

    fun hasNoPendingSubscription() = subscriptions.all(JobSubscription::hasJobCompleted)

    fun clearSubscriptions() = subscriptions.clear()

    fun sendJobToService(context: Context, jobId: Long, jobType: JobType) {
        val existingSubscription = subscriptions.find { it.jobId == jobId }
        if (existingSubscription != null) {
            existingSubscription.resetEvents()
        } else {
            subscriptions.add(JobSubscription(jobId))
        }

        context.startService(
            Intent(context, HashingService::class.java)
                .putExtra(KEY_HASHJOB_ID, jobId)
                .putExtra(KEY_JOB_TYPE, jobType.ordinal)
        )
    }
}
