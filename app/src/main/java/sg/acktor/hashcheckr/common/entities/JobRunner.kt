package sg.acktor.hashcheckr.common.entities

import android.net.Uri

class JobRunner(
    val id: Long,
    val algorithm: String,
    val fileId: Long,
    val fileName: String,
    val fileUri: Uri,
    var generatedHashSum: String?
)
