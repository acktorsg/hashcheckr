package sg.acktor.hashcheckr.common

const val KEY_MISSING_ALGORITHMS = "missing_algorithms"
const val KEY_HASHJOB_ID = "hashjob_id"
const val KEY_UI_THEME = "uiTheme"
const val KEY_JOB_TYPE = "job_type"

const val LABEL_HASHSUM = "Hashsum from HashCheckr"

const val DATABASE_NAME = "hashcheckr"

const val CHANNEL_HASHJOB_SERVICE = "hashjob_service"

const val MAX_ADD_JOB_COUNT = 3
