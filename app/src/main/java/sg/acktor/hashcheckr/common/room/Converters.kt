package sg.acktor.hashcheckr.common.room

import android.net.Uri
import androidx.room.TypeConverter
import java.util.Date

class UriConverter {
    @TypeConverter
    fun uriToString(uri: Uri) = uri.toString()

    @TypeConverter
    fun stringToUri(strUri: String): Uri? = Uri.parse(strUri)
}

class DateTimeConverter {
    @TypeConverter
    fun dateToLong(date: Date) = date.time

    @TypeConverter
    fun longToDate(timeInMillis: Long): Date = Date(timeInMillis)
}
