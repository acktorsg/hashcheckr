package sg.acktor.hashcheckr.common

import android.content.Context
import androidx.room.Room
import sg.acktor.hashcheckr.common.room.HashCheckrDatabase
import sg.acktor.hashcheckr.common.room.migration.Migration1to2
import sg.acktor.hashcheckr.common.room.migration.Migration2to3

object JobRepository {
    private var DB_INSTANCE: HashCheckrDatabase? = null

    fun getDatabase(context: Context) = DB_INSTANCE ?: synchronized(this) {
        Room.databaseBuilder(context, HashCheckrDatabase::class.java, DATABASE_NAME)
            .addMigrations(Migration1to2(), Migration2to3())
            .build().also(this::DB_INSTANCE::set)
    }
}