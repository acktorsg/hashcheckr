package sg.acktor.hashcheckr.common.extensions

import android.content.Context
import android.net.Uri
import androidx.documentfile.provider.DocumentFile
import java.io.FileNotFoundException
import java.io.IOException

fun Context.checkFileAvailability(uri: Uri): Exception? {
    val documentFile = DocumentFile.fromSingleUri(applicationContext, uri)
        ?: return IOException()

    if (!documentFile.exists()) return FileNotFoundException()
    if (!documentFile.canRead()) return IOException()

    return null
}