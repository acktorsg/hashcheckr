package sg.acktor.hashcheckr.common.room.migration

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.util.JsonReader
import android.util.JsonWriter
import androidx.documentfile.provider.DocumentFile
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import java.io.File
import java.io.StringReader
import java.io.StringWriter

class Migration2to3 : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.beginTransaction()

        val cursor = database.query(
                SupportSQLiteQueryBuilder.builder("hashJob")
                        .columns(arrayOf("id", "task")).create()
        )
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val values = ContentValues(1)

                do {
                    updateTask(database,
                               cursor.getLong(cursor.getColumnIndex("id")),
                               cursor.getString(cursor.getColumnIndex("task")),
                               values)
                } while (cursor.moveToNext())
            }

            cursor.close()
        }

        database.setTransactionSuccessful()
        database.endTransaction()
    }

    private fun updateTask(database: SupportSQLiteDatabase,
                           id: Long, strTask: String, contentValues: ContentValues) {
        val jsonReader = JsonReader(StringReader(strTask))
        val strWriter = StringWriter()
        val jsonWriter = JsonWriter(strWriter)

        jsonReader.beginObject()
        jsonWriter.beginObject()

        jsonWriter.name(jsonReader.nextName())
        when (val taskType = jsonReader.nextString()) {
            "HashFile" -> {
                jsonWriter.value(taskType)

                jsonReader.nextName()
                val documentFile = DocumentFile.fromFile(File(jsonReader.nextString()))
                val fileName = documentFile.name
                val fileUri = documentFile.uri.toString()

                jsonWriter.name("fileName")
                jsonWriter.value(fileName)
                jsonWriter.name("fileUri")
                jsonWriter.value(fileUri)

                jsonWriter.name("hashSumResult")
                jsonReader.nextName()
                jsonWriter.value(jsonReader.nextString())

                jsonReader.endObject()
                jsonReader.close()

                jsonWriter.endObject()
                jsonWriter.close()

                contentValues.put("task", strWriter.toString())

                database.update("hashJob", SQLiteDatabase.CONFLICT_IGNORE, contentValues,
                                "id = ?", arrayOf(id))
            }
            else       -> {
                jsonWriter.value(taskType)
                database.delete("hashJob", "id = ?", arrayOf(id))

                jsonReader.close()
                jsonWriter.endObject()
                jsonWriter.close()
            }
        }
    }
}