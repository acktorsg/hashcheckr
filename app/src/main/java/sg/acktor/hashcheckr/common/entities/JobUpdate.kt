package sg.acktor.hashcheckr.common.entities

class FileHashSumDelete(val fileId: Long, val algorithm: String)

class EntityId(val id: Long)

class LastModifiedUpdate(val id: Long, val lastModified: Long)
