package sg.acktor.hashcheckr.common.entities

import android.net.Uri
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.Date

enum class JobType { GENERATE, VALIDATE }

@Entity(
    foreignKeys = [ForeignKey(
        entity = File::class,
        parentColumns = ["id"],
        childColumns = ["fileId"],
        onDelete = ForeignKey.RESTRICT
    )],
    indices = [Index(value = ["fileId"])]
)
class HashingJob(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val algorithm: String,
    // DO NOT replace this with defaultValue in ColumnInfo. Otherwise attribute will be converted to
    // TEXT & time comparisons won't work
    val creationDateTime: Date = Date(),
    val fileId: Long
)

@Entity
class GenerateJob(@PrimaryKey val id: Long)

@Entity
class ValidateJob(@PrimaryKey val id: Long, val hashSum: String)

@Entity(
    primaryKeys = ["fileId", "algorithm"],
    foreignKeys = [ForeignKey(
        entity = File::class,
        parentColumns = ["id"],
        childColumns = ["fileId"],
        onDelete = ForeignKey.RESTRICT
    )],
    indices = [Index(value = ["fileId"])]
)
class GeneratedHashSum(val fileId: Long, val algorithm: String, val generatedHashSum: String)

@Entity
data class File(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String,
    val uri: Uri,
    val lastModified: Long
)
