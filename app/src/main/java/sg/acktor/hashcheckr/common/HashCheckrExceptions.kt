package sg.acktor.hashcheckr.common

class NoFileException : Exception()

class NoHashSumException : Exception()

class CreateJobException : Exception()