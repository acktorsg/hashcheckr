package sg.acktor.hashcheckr.common.extensions

import androidx.fragment.app.Fragment
import sg.acktor.hashcheckr.MainActivity

val Fragment.mainActivity
    get() = (activity as? MainActivity)
        ?: throw IllegalStateException("Fragment $this not attached to MainActivity")