package sg.acktor.hashcheckr.common.room.migration

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import java.io.StringReader
import java.io.StringWriter

class Migration1to2 : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.beginTransaction()

        database.execSQL("ALTER TABLE hashjob RENAME TO tempHashJob")
        database.execSQL("CREATE TABLE hashjob (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, algorithm TEXT NOT NULL, task TEXT NOT NULL, creationDateTime INTEGER NOT NULL)")

        val cursor = database.query(
                SupportSQLiteQueryBuilder.builder("tempHashJob")
                        .columns(arrayOf("id", "task", "creationDateTime")).create()
        )
        if (cursor.moveToFirst()) {
            val contentValues = ContentValues()

            do {
                contentValues.put("id", cursor.getLong(cursor.getColumnIndex("id")))
                contentValues.put("creationDateTime",
                                  cursor.getLong(cursor.getColumnIndex("creationDateTime")))

                val (algorithm, task) = getAlgoAndTask(cursor.getString(cursor.getColumnIndex("task")))
                contentValues.put("algorithm", algorithm)
                contentValues.put("task", task)

                database.insert("hashjob", SQLiteDatabase.CONFLICT_IGNORE, contentValues)
            } while (cursor.moveToNext())
        }

        cursor.close()
        database.execSQL("DROP TABLE tempHashJob")

        database.setTransactionSuccessful()
        database.endTransaction()
    }

    private fun getAlgoAndTask(json: String): Pair<String, String> {
        val stringWriter = StringWriter()
        val writer = JsonWriter(stringWriter)
        writer.beginObject()

        val reader = JsonReader(StringReader(json))
        reader.beginObject()

        reader.nextName()
        val algorithm = reader.nextString()

        var result: String? = null
        var comparisonResult: Boolean? = null
        val nextName = reader.nextName()
        if (nextName != "taskType") {
            when (nextName) {
                "result"            -> result = reader.nextString()
                "comparison_result" -> comparisonResult = reader.nextBoolean()
            }

            reader.nextName()
        }

        writer.name("taskType")
        when (reader.nextString()) {
            "HashFile"    -> {
                reader.nextName()
                val filePath = reader.nextString()

                writer.value("HashFile")

                writer.name("filePath")
                writer.value(filePath)

                writer.name("result")
                writer.value(result ?: "")
            }
            "HashText"    -> {
                reader.nextName()
                val input = reader.nextString()

                writer.value("HashText")

                writer.name("input")
                writer.value(input)

                writer.name("result")
                writer.value(result ?: "")
            }
            "CompareHash" -> {
                reader.nextName()
                val filePath = reader.nextString()

                reader.nextName()
                val hashSum = reader.nextString()

                writer.value("CheckHash")

                writer.name("filePath")
                writer.value(filePath)

                writer.name("hashSum")
                writer.value(hashSum)

                writer.name("result")
                writer.value(comparisonResult?.toString() ?: "")
            }
        }

        reader.endObject()
        reader.close()

        writer.endObject()
        writer.close()

        return algorithm to stringWriter.toString()
    }
}