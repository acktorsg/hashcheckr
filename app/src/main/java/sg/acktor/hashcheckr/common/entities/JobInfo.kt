package sg.acktor.hashcheckr.common.entities

import java.util.Date

sealed class JobInfo(
    val id: Long,
    val algorithm: String,
    val creationDateTime: Date,
    val fileId: Long,
    val fileName: String,
    val generatedHashSum: String?
) {
    open fun areContentsTheSame(other: JobInfo) = algorithm == other.algorithm
            && creationDateTime == other.creationDateTime && fileId == other.fileId
            && fileName == other.fileName && generatedHashSum == other.generatedHashSum
}

class GenerateJobInfo(
    id: Long,
    algorithm: String,
    creationDateTime: Date,
    fileId: Long,
    fileName: String,
    generatedHashSum: String?
) : JobInfo(id, algorithm, creationDateTime, fileId, fileName, generatedHashSum) {
    override fun areContentsTheSame(other: JobInfo) = other is GenerateJobInfo
            && super.areContentsTheSame(other)
}

class ValidateJobInfo(
    id: Long,
    algorithm: String,
    creationDateTime: Date,
    val hashSum: String,
    fileId: Long,
    fileName: String,
    generatedHashSum: String?
) : JobInfo(id, algorithm, creationDateTime, fileId, fileName, generatedHashSum) {
    override fun areContentsTheSame(other: JobInfo) = other is ValidateJobInfo
            && hashSum == other.hashSum
            && super.areContentsTheSame(other)
}
