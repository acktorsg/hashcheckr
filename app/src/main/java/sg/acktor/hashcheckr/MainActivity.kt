package sg.acktor.hashcheckr

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import sg.acktor.aboutpage.AboutFragment
import sg.acktor.aboutpage.getLicenses
import sg.acktor.acktorsdk.ui.snackbar.SnackbarAction
import sg.acktor.hashcheckr.databinding.ActivityMainBinding
import sg.acktor.hashcheckr.features.generatehash.HashEngine
import sg.acktor.hashcheckr.features.home.HomeFragment
import sg.acktor.hashcheckr.features.missingalgorithms.MissingAlgorithmFragment
import sg.acktor.hashcheckr.features.settings.SettingsFragment

class MainActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityMainBinding

    private lateinit var homeFragment: HomeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        var frag = supportFragmentManager.findFragmentByTag(HomeFragment.TAG)
        if (frag == null) {
            val fragmentTag: String

            val missingAlgorithms = HashEngine.checkAllAlgorithmsExist()
            if (missingAlgorithms.isEmpty()) {
                frag = HomeFragment()
                fragmentTag = HomeFragment.TAG
            } else {
                frag = MissingAlgorithmFragment.create(missingAlgorithms.toTypedArray())
                fragmentTag = MissingAlgorithmFragment.TAG
            }

            supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentHolder, frag, fragmentTag)
                .commit()
        }

        if (frag is HomeFragment) {
            homeFragment = frag
        }
    }

    fun showAboutUi() {
        if (supportFragmentManager.findFragmentByTag(AboutFragment.TAG) == null) {
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(
                    R.anim.fragment_enter_from_right, R.anim.fragment_exit_fade,
                    R.anim.fragment_enter_fade, R.anim.fragment_exit_to_right
                )
                .add(
                    R.id.fragmentHolder,
                    AboutFragment.create(getLicenses(R.raw.licenses)),
                    AboutFragment.TAG
                )
                .hide(homeFragment)
                .addToBackStack(null)
                .commit()
        }
    }

    fun showSettingsUi() {
        if (supportFragmentManager.findFragmentByTag(SettingsFragment.TAG) == null) {
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(
                    R.anim.fragment_enter_from_right, R.anim.fragment_exit_fade,
                    R.anim.fragment_enter_fade, R.anim.fragment_exit_to_right
                )
                .add(R.id.fragmentHolder, SettingsFragment(), SettingsFragment.TAG)
                .hide(homeFragment)
                .addToBackStack(null)
                .commit()
        }
    }

    fun showMessage(message: String, snackbarAction: SnackbarAction? = null) {
        val snackbar = Snackbar.make(viewBinding.root, message, Snackbar.LENGTH_SHORT)
        if (snackbarAction != null) {
            snackbar.setAction(snackbarAction.titleId, snackbarAction.listener)
        }

        snackbar.show()
    }
}
